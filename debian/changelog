prometheus-mailexporter (1.0+git20211213.f46e4f8-1) UNRELEASED; urgency=medium

  [ Vinay Keshava ]
  * Team upload.
  * Remove constraints unnecessary since buster
  * New upstream version 1.0+git20211213.f46e4f8

 -- Vinay Keshava <vinaykeshava@disroot.org>  Mon, 30 Oct 2023 22:34:36 +0530

prometheus-mailexporter (1.0+git20190716.c60d197-2) UNRELEASED; urgency=medium

  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Debian Janitor <janitor@jelmer.uk>  Wed, 25 Mar 2020 01:32:55 +0000

prometheus-mailexporter (1.0+git20190716.c60d197-1) unstable; urgency=medium

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Anthony Fok ]
  * New upstream version 1.0+git20190716.c60d197
    Migrate to new library version, changing prometheus.Handler()
    to promhttp.Handler()  (Closes: #952352)
  * Bump golang-github-prometheus-client-golang-dev (>= 1.2.1)
    to ensure it has the new API prometheus/promhttp
  * Set debian/watch to track git HEAD for now, and remove uupdate
  * debian/gbp.conf: Set debian-branch to debian/sid for DEP-14 conformance
  * Update Maintainer email address to team+pkg-go@tracker.debian.org
  * Bump debhelper dependency to "Build-Depends: debhelper-compat (= 12)"
  * Bump Standards-Version to 4.5.0 (no change)
  * Add "Rules-Requires-Root: no" to debian/control
  * Use "dh_auto_install -- --no-source" to simply debian/rules

 -- Anthony Fok <foka@debian.org>  Fri, 28 Feb 2020 08:54:38 -0700

prometheus-mailexporter (1.0-2) unstable; urgency=medium

  * Apply "cme fix dpkg" to debian/control and debian/copyright,
    bumping Standards-Version to 4.1.3,
    setting Priority to optional, and
    adding Testsuite: autopkgtest-pkg-go, etc.
  * Use debhelper (>= 11)
  * Depend on golang-any instead of golang-go
  * Depend on golang-github-fsnotify-fsnotify-dev
    instead of the obsolete golang-fsnotify-dev transitional package.
  * Depend on golang-gopkg-yaml.v2-dev
    instead of the deprecated golang-yaml.v2-dev transitional package.
  * Depend on golang-github-prometheus-client-golang-dev
    instead of the deprecated golang-prometheus-client-dev
    transitional package.
  * Add myself to the list of Uploaders

 -- Anthony Fok <foka@debian.org>  Tue, 06 Mar 2018 21:47:08 -0700

prometheus-mailexporter (1.0-1) unstable; urgency=medium

  * New upstream version 1.0

 -- Jonas Große Sundrup <cherti+debian@letopolis.de>  Mon, 16 Jan 2017 17:06:05 +0100

prometheus-mailexporter (0.4-1) unstable; urgency=medium

  * New upstream version 0.4

 -- Jonas Große Sundrup <cherti+debian@letopolis.de>  Mon, 09 Jan 2017 19:12:29 +0100

prometheus-mailexporter (0.3-1) unstable; urgency=medium

  * Initial release (Closes: #847517)

 -- Jonas Große Sundrup <cherti+debian@letopolis.de>  Wed, 14 Dec 2016 00:32:27 +0100
